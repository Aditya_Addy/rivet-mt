#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

#include "HepMC3/ReaderAsciiHepMC2.h"
#include "HepMC3/ReaderAscii.h"
#include "HepMC3/GenEvent.h"
#include "HepMC3/Print.h"
#include<iostream>
#include "Rivet/AnalysisHandler.hh"
#include<random>
#include <pthread.h> 
using namespace HepMC3;

using namespace std;

void read(string const & fname)
{
  int nev = 0;
     
    Rivet::AnalysisHandler *ah = new Rivet::AnalysisHandler();


    ah->addAnalysis("MC_TTBAR");
    ah->addAnalysis("MC_JETS");
    ah->addAnalysis("MC_GENERIC");

        ReaderAsciiHepMC2 reader(fname);
        GenEvent evt[10005];
        GenEvent evt_1;
        while (!reader.failed()) 
        {
          //cout<<nev<<"\n";
            //GenEvent evt;
            //reader.read_event(evt[nev]);
            reader.read_event(evt[nev]);
            cout<<nev<<endl;
            ah->analyze(evt[nev]);      
            //cout << evt<<'\n';
            if (reader.failed())  {
                printf("End of file reached. Exit.\n");
                break;
            }
            nev++;
        }
    
    ah->finalize();
    ah->writeData("output.yoda");



}


int main()
{
  read("../event.hepmc");
  return 0;
}
