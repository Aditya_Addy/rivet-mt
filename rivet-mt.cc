#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

#include "HepMC3/ReaderAsciiHepMC2.h"
#include "HepMC3/ReaderAscii.h"
#include "HepMC3/GenEvent.h"
#include "HepMC3/Print.h"
#include <iostream>
#include "Rivet/AnalysisHandler.hh"
#include <random>
#include <pthread.h>
#include <queue>
using namespace HepMC3;

using namespace std;

int event_read = 0;
int event_processed = 0;
int end_of_file = 0;
//GenEvent evt[10005];
queue<GenEvent> eventQueue;
pthread_mutex_t queue_lock;

void *process_event(void *p)
{

  Rivet::AnalysisHandler *ah = new Rivet::AnalysisHandler();
  ah->addAnalysis("MC_TTBAR");
  ah->addAnalysis("MC_JETS");
  ah->addAnalysis("MC_GENERIC");

  while (!end_of_file || !eventQueue.empty())
  {
    //cout<<eventQueue.size()<<endl;
    if (!eventQueue.empty())
    {
      GenEvent event = eventQueue.front();
      pthread_mutex_lock(&queue_lock);
      eventQueue.pop();
      pthread_mutex_unlock(&queue_lock);
      ah->analyze(event);
      event_processed++;
      //cout << event_processed << " event read " << event_read << "  size " << eventQueue.size() << endl;
    }
    else
    {
      cout<<"";
    //  //cout << "queue is empty " << eventQueue.size() << endl;
    }
  }
  cout << "Finalizing\n";
  ah->finalize();
  ah->writeData("outputMt.yoda");
}

void *read_from_file(void *p)
{
  ReaderAsciiHepMC2 reader("../event.hepmc");

  while (!reader.failed())
  {
    GenEvent event;
    reader.read_event(event);
    pthread_mutex_lock(&queue_lock);
    eventQueue.push(event);
    pthread_mutex_unlock(&queue_lock);

    event_read++;
    //cout<<"event_read "<<event_read<<endl;
    if (reader.failed())
    {
      cout << "End of file reached. Exit.\n";
      end_of_file = 1;
    }
  }
}

int main()
{
  pthread_t threads[2];

  threads[0] = 0;
  threads[1] = 1;
  pthread_create(&threads[1], NULL, read_from_file, (void *)&threads[1]);
  pthread_create(&threads[0], NULL, process_event, (void *)&threads[0]);
  pthread_join(threads[0], NULL);
  pthread_join(threads[1], NULL);
  return 0;
}
