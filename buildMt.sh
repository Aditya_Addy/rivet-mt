
path=/home/addy/Desktop/gsoc2020/rivet/dev-rivet-hepmc3/local

g++ -O3 -g rivet-mt.cc -o mt-rivet -march=native -mtune=native  -DNDEBUG  -std=c++17 \
    -I${path}/include \
    -L${path}/lib64 -lHepMC3 -lRivet \
    -lpthread